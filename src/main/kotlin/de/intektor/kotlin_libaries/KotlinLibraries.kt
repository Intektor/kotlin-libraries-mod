package de.intektor.kotlin_libaries

import net.minecraftforge.fml.common.Mod

@Mod(KotlinLibraries.ID)
class KotlinLibraries {

    companion object {
        const val ID = "kotlin_libraries"
    }
}